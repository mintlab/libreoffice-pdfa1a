pdfa1a.oxt: $(shell find src/ -type f)
	(cd src && zip -r ../pdfa1a.oxt .)

all: pdfa1a.oxt

install: pdfa1a.oxt
	sudo unopkg add --shared pdfa1a.oxt --force

clean:
	rm -f pdfa1a.oxt

test: all clean
	libreoffice

# These targets don't create output files, so always run them
# even if a file by that name exists.
.PHONY: all install clean test
